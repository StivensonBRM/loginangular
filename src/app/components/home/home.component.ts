import { Component, OnInit } from '@angular/core';
import { SessionsService } from '../../services/sessions.service';
import { Router} from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import {Query} from '../../models/query';
import {LogSession} from '../../models/log-session';
import {Resource} from '../../models/resource';



@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  public user: any;
  public token: string;
  public query: Query;
  public log_session: LogSession;
  public resource: Resource;
  public date = new Date();
  constructor(
    private session_service: SessionsService,
    private router: Router,
    private cookie: CookieService
  ) {
    this.query = new Query();
    this.log_session = new LogSession();

  }

  ngOnInit() {
    console.log(this.log_session);
    this.session_service.getData().subscribe(
      result => {
        this.user = result;
        this.redirect();
      }, error => {
        this.cookie.deleteAll();
        this.router.navigate(['login']);
        console.log(<any>error.error);
      }
    );
  }



  private redirect(){
    var rol = this.cookie.get('X-Rol-Mind');
    var client = this.cookie.get('X-Client-Mind');

    if (rol != null && client != null) {
      if (client == 'Davivienda') {
        if (rol == 'Asesores' || rol == 'Coordinadores' ) {
          console.log("redirect");
          location.href ="http://localhost:4201";
        }
      } else {
        this.router.navigate(['home/logout']);
      }
    }
  }
}
