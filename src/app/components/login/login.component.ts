import { Component, OnInit } from '@angular/core';
import { UsersService} from '../../services/users.service';
import { User } from '../../models/user';
import { Router} from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import {forEach} from '@angular/router/src/utils/collection';
import {SessionsService} from '../../services/sessions.service';
import {LogSession} from '../../models/log-session';
import {Resource} from '../../models/resource';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [UsersService]
})
export class LoginComponent implements OnInit {
  public user: User;
  public log_session: LogSession;
  public resource: Resource;
  public menbers_array=[];
  public date = new Date();

  constructor(
    private user_service: UsersService,
    private session_service: SessionsService,
    private router: Router,
    private cookie: CookieService
  ) {
    this.user = new User();
    this.log_session = new LogSession();
    this.resource = new Resource();
  }

  ngOnInit() {
    this.session_service.getData().subscribe(
      result => {
        this.router.navigate(['home']);
      }, error => {
        console.log(<any>error.error);
      }
    );
  }

  onSubmit() {
    this.user_service.addUser(this.user).subscribe(
      result => {
        this.log_session = result.data;
        console.log('HOLA');
        console.log(this.log_session.members);
        console.log(this.log_session.members[0]['rol']);
        console.log(this.log_session.members[0]['client']);
        this.cookie.set('X-Rol-Mind', this.log_session.members[0]['rol']);
        this.cookie.set('X-Client-Mind', this.log_session.members[0]['client']);
        this.cookie.set('X-Username-Mind', this.user.username);
        this.cookie.set('X-Token-Mind', this.log_session.session_token);
        console.log("test");
        this.router.navigate(['home']);
      }, error => {
        console.log(<any>error.error);
      }
    );
  }



}
