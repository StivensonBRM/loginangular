import { Component, OnInit } from '@angular/core';
import { Router} from '@angular/router';
import {SessionsService} from '../../services/sessions.service';
import {LogSession} from '../../models/log-session';
import {Resource} from '../../models/resource';
import {CookieService} from 'ngx-cookie-service';
import {Query} from '../../models/query';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.css']
})
export class LogoutComponent implements OnInit {
  public query: Query;
  public user: any;
  public log_session: LogSession;
  public resource: Resource;
  public date = new Date();
  public data_resource = new Array();

  constructor(
    private router: Router,
    private session_service: SessionsService,
    private cookie: CookieService
  ) {
    this.query = new Query();
    this.log_session = new LogSession();
    this.resource = new Resource();
  }

  ngOnInit() {
    this.session_service.getData().subscribe(
      result => {
        this.user = result;
        this.session_service.logout(this.user.username).subscribe(
          result => {
            this.cookie.deleteAll();
            this.router.navigate(['login']);
          }
        );
      }
    );

  }




}
