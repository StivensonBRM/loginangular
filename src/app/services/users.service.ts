import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, map, tap} from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { User } from '../models/user';
import {Resource} from '../models/resource';


const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'}
  )};

@Injectable({
  providedIn: 'root'
})
export class UsersService {
  public API_URL = environment.API_URL;

  constructor(private http: HttpClient) {}

  addUser (user: User): Observable<Resource> {
    return this.http.post<Resource>(this.API_URL+'session', user, httpOptions);
  }

}
