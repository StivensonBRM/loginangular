import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, map, tap} from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { CookieService } from 'ngx-cookie-service';

@Injectable({
  providedIn: 'root'
})
export class SessionsService {
  public API_URL = environment.API_URL;

  constructor(
    private http: HttpClient,
    private cookie: CookieService) { }

  public httpOptions = {
    headers: new HttpHeaders({'Accept': 'application/json', 'Authorization': this.cookie.get('X-Token-Mind')}
    )};

  getData() {
    return this.http.get(this.API_URL+'user', this.httpOptions);
  }

  logout(username) {
    return this.http.get(this.API_URL+'logout/'+username, this.httpOptions);
  }


}
