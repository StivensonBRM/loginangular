export class LogSession {
  username: string;
  session_token: string;
  members: string;
  distinguishedname: string;
  session_state: number;
}
